<?php

/**
 * @file
 *
 * Filter to only show items that have a specific value in a specific field
 *
 * This is heavily inspired by the excellent blog post from Trellon
 * http://www.trellon.com/content/blog/apachesolr-and-facetapi
 */

/**
 * Plugin that only shows items that have a specific value in a specific field.
 */
class FacetapiFilterFieldValue extends FacetapiFilter {

  /**
   * Filters facet items.
   */
  public function execute(array $build) {
    $field = $this->settings->settings['field'];
    $negate = $this->settings->settings['negate'];
    $values_string = $this->settings->settings['values'];
    $values_array = drupal_explode_tags($values_string);

    $facets = facetapi_get_enabled_facets($this->settings->searcher);
    $facet = $facets[$this->settings->facet];
    $entity_type = $facet['map options']['field']['entity_type'];

    // Include item if the value on the field is one of the user listed values.
    $filtered_build = array();
    foreach ($build as $key => $item) {
      $include = FALSE;
      foreach ($values_array as $values_item) {
        if (!empty($values_item)) {
          $entity = entity_load_single($entity_type, $key);
          if (strpos($field, ':') === FALSE) {
            // We are comparing the value on a normal field.
            $result = $entity->{$field};
            foreach ($result[LANGUAGE_NONE] as $value) {
              $value = array_values($value);
              // The actual comparison takes place here.
              $include = ($value[0] == $values_item);
              // Bail out as soon as we have positive confirmation.
              if ($include) {
                break 2;
              }
            }
          }
          else {
            // We are comparing the value on a (sub-)field on a field entity that is fieldable.
            $include = $this->getIncludeStatus($field, $values_item, $key);
            // Bail out as soon as we have positive confirmation.
            if ($include) {
              break 1;
            }
          }
        }
      }
      if ($negate) {
        $include = !$include;
      }
      if ($include) {
        $filtered_build[$key] = $item;
      }
    }

    return $filtered_build;
  }

  /**
   * Adds settings to the filter form.
   */
  public function settingsForm(&$form, &$form_state) {
    // Skip non-fieldable entities.
    $facets = facetapi_get_enabled_facets($this->settings->searcher);
    $facet = $facets[$this->settings->facet];
    if (!isset($facet['map options']['field']['entity_type'])) {
      return;
    }

    // Determine the bundles whose fields are to added to the list.
    $entity_type = $facet['map options']['field']['entity_type'];
    $property_info = entity_get_property_info($entity_type);
    $property_bundles = array_keys($property_info['bundles']);
    $entity_bundles = array_keys(field_info_bundles($entity_type));
    $bundles = array_intersect($property_bundles, $entity_bundles);
    if ($bundles == array()) {
      return;
    }

    // Create the list of fields.
    $options = array();
    foreach ($bundles as $bundle) {
      $fields = field_info_instances($entity_type, $bundle);
      foreach ($fields as $field_name =>$field) {
        $options[$field_name] = $field_name;
        // Also add (sub-)fields from field entities that are fieldable.
        $field_info = field_info_field($field_name);
        $entity_subtype = $field_info['type'];
        if (entity_type_is_fieldable($entity_subtype)) {
          $property_subinfo = entity_get_property_info($entity_subtype);
          if (isset($property_subinfo['bundles'])) {
            $property_subbundles = array_keys($property_subinfo['bundles']);
            $entity_subbundles = array_keys(field_info_bundles($entity_subtype));
            $subbundles = array_intersect($property_subbundles, $entity_subbundles);
            if ($subbundles == array()) {
              continue;
            }
            foreach ($subbundles as $subbundle) {
              $subfields = field_info_instances($entity_subtype, $subbundle);
              foreach ($subfields as $subfield_name => $subfield) {
                // Store them recognizable with the field and subfield name seperated by a ':'.
                $options[$field_name . ':' . $subfield_name] = $field_name . ':' . $subfield_name;
              }
            }
          }
        }
      }
    }
    asort($options);

    $form['field'] = array(
      '#title' => t('Field'),
      '#type' => 'select',
      '#options' => $options,
      '#description' => t("The field on the item to use to include items."),
      '#default_value' => $this->settings->settings['field'],
    );
    $form['negate'] = array(
      '#title' => t('Negate'),
      '#type' => 'checkbox',
      '#description' => t('Whether to negate the field values to exclude items.'),
      '#default_value' => $this->settings->settings['negate'],
    );
    $form['values'] = array(
      '#title' => t('Values'),
      '#type' => 'textfield',
      '#description' => t('Comma separated list of field values used to include items. Use tid for taxonomy term references, entity_id for entity references etc...'),
      '#default_value' => $this->settings->settings['values'],
    );
  }

  /**
   * Returns an array of default settings.
   */
  public function getDefaultSettings() {
    return array(
      'field' => '',
      'negate' => FALSE,
      'values' => '',
    );
  }

  /**
   * Determine the include status of an item based
   * on the value of a field entity's (sub-)field.
   * 
   * @param string $field
   * @param string $values_item
   * @param string $key
   * @return boolean
   */
  protected function getIncludeStatus($field, $values_item, $key) {
    // TODO: This needs some entity and field type independent redoing.
    list($mainfield, $subfield) = explode(':', $field, 2);
    $mainfield_info = field_info_field($mainfield);
    $subfield_info = field_info_field($subfield);
    $entityQuery = new EntityFieldQuery();
    switch ($mainfield_info['type']) {
      case 'relation':
        switch ($subfield_info['type']) {
          case 'taxonomy_term_reference':
            $entities = $entityQuery->entityCondition('entity_type', $mainfield_info['type'])
              ->fieldCondition($subfield, 'tid', $values_item)
              ->fieldCondition('endpoints', 'entity_id', $key)
              ->execute();
            $break;
          default:
        }
        $break;
      default:
    }
    return (isset($entities[$mainfield_info['type']]) && !empty($entities[$mainfield_info['type']]));
  }
}
